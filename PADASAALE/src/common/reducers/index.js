import { combineReducers } from 'redux';
import CommonReducer from './CommonReducer';
import AuthReducer from '../../screens/auth/reducer/AuthReducer';
import ActivityHomeReducer from '../../screens/home/activity/reducer/ActivityHomeReducer';
import CircularHomeReducer from '../../screens/home/circular/reducer/CircularHomeReducer';
import ResultHomeReducer from '../../screens/home/result/reducer/ResultHomeReducer';

export default combineReducers({
  common: CommonReducer,
  authData: AuthReducer,
  activityHome: ActivityHomeReducer,
  circularHome: CircularHomeReducer,
  resultHome: ResultHomeReducer,
});