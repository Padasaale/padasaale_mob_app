import { all } from 'redux-saga/effects';
import authSagas from '../../screens/auth/saga/AuthSagas';
import activityHomeSaga from '../../screens/home/activity/saga/ActivityHomeSagas';
import circularHomeSaga from '../../screens/home/circular/saga/CircularHomeSagas';
import resultHomeSaga from '../../screens/home/result/saga/ResultHomeSagas';

export default function* () {
  yield all([
    ...authSagas,
    ...activityHomeSaga,
    ...circularHomeSaga,
    ...resultHomeSaga,
  ]);
}