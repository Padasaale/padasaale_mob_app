import activity from '../images/activity.png';
import circular from '../images/circular.png';
import result from '../images/result.png'
import note from '../images/note.png'
import attach from '../images/attach.png'

export const Icons = {
  activity,
  circular,
  result,
  note,
  attach,
}