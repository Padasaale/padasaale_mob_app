import React from 'react'
import { Image } from 'react-native'
import { Icons } from './Icons'
const IconComponent = props => {
    const { name, size, height, width } = props;
    return (
        <Image
            resizeMode='contain'
            source={Icons[name]}
            style={{ width: size | width, height: size | height }} />
    );
}
export default IconComponent;