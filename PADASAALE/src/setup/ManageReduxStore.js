import {
  SHARED_PREFERENCE_KEYS,
  SHARED_PREFERENCE_ACTION_CONSTANTS
} from '../common/constants/AppConstants'
import { SHARED_PREFERENCE } from '../utils/SharedPreferences'

export const updateReduxStore = props => {
  const keys = [
    SHARED_PREFERENCE_KEYS.USER_SIGNED_IN,
    SHARED_PREFERENCE_KEYS.USER_ID,
    SHARED_PREFERENCE_KEYS.USER_DETAILS
  ];
  SHARED_PREFERENCE.accessor.getMultiple(keys, user => {
    const userData = {
      [SHARED_PREFERENCE_KEYS.USER_ID]: user.USER_ID ? user.USER_ID : null,
      [SHARED_PREFERENCE_KEYS.USER_SIGNED_IN]: user.USER_SIGNED_IN && user.USER_SIGNED_IN === 'true' ? true : false,
      [SHARED_PREFERENCE_KEYS.USER_DETAILS]: user.USER_DETAILS ? JSON.parse(user.USER_DETAILS) : null
    };
    props.dispatch(
      {
        type: SHARED_PREFERENCE_ACTION_CONSTANTS.UPDATE_USER_DATA,
        payload: userData
      }
    );
  });
}