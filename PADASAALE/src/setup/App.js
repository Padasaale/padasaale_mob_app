import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import configureStore from './ConfigureStore'
import NavigationStack from '../navigation'
import { updateReduxStore } from './ManageReduxStore';

export const store = configureStore();

export default AppContainer = (props) => {
  useEffect(() => {
    setTimeout(() => {
      updateReduxStore(store);
    }, 0)
  })

  return (
    <Provider store={store}>
      <NavigationStack />
    </Provider>
  );
}