import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import sagas from '../common/sagas'
import reducers from '../common/reducers'

export default configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  let store = createStore(reducers, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(sagas);
  return store;
}