import { takeEvery, call, put } from 'redux-saga/effects';
import { GET_CIRCULARS_ACTION_CONSTANTS } from '../constants/constants';
import { getCircularsApi } from '../api/CircularHomeApis'
import { getCircularsSuccessActions } from '../actions/CircularHomeActions';

export default [
  takeEvery(GET_CIRCULARS_ACTION_CONSTANTS.GET_CIRCULARS_REQUEST_ACTION, getCircularsFunSaga),
];

function* getCircularsFunSaga(action) {
  const { apiInput } = action.payload;
  const getCircularsApiResponse = yield call(getCircularsApi, apiInput);
  if (getCircularsApiResponse && getCircularsApiResponse.status === 'SUCCESS') {
    yield put(getCircularsSuccessActions(getCircularsApiResponse));
  }
}