import { networkHandler } from '../../../../utils/network/NetworkHandler';
import { CIRCULAR_API_ENDPOINT } from '../constants/apiConstants'

export const getCircularsApi = async (apiInput) => {
  const { userId, userType } = apiInput;
  try {
    const url = `${CIRCULAR_API_ENDPOINT.GET_CIRCULAR_API_END_POINT}?assignedTo=${userId}`;
    const getCircularsResponse = await networkHandler.get(url);
    return getCircularsResponse.data;
  } catch (error) {
    console.log('Get circulars api error', error);
    return error;
  }
}