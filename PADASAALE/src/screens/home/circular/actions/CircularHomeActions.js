import { GET_CIRCULARS_ACTION_CONSTANTS } from '../constants/constants';

export const getCircularsRequestActions = (apiInput) => ({
  type: GET_CIRCULARS_ACTION_CONSTANTS.GET_CIRCULARS_REQUEST_ACTION,
  payload: {
    apiInput,
  }
});

export const getCircularsSuccessActions = (response) => ({
  type: GET_CIRCULARS_ACTION_CONSTANTS.GET_CIRCULARS_SUCCESS_ACTION,
  payload: response,
});