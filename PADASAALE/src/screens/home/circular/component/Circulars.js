import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text } from 'react-native'
import lodash from 'lodash'
import { getCircularsRequestActions } from '../actions/CircularHomeActions'
import { HOME_SCREEN_TABS_NAME } from '../../../common/Constants/Constants'
import { NoticeBoard } from '../../../common/NoticeBoardCommonComponents/NoticeBoard'
import { SIZES } from '../../../../utils/ThemeProvider/ThemeProvider'

class Circulars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '5f8a7f846e737a4001fe1fce',
      userType: 'faculty',
      // userId: '5f4a2cbd006c3b0aa299e73e',
      // userType: 'student'
    }
  }

  componentDidMount() {
    const { userId, userType } = this.state;
    const apiInput = {
      userId: userId,
      userType: userType,
    }
    this.props.getCircularsAction(apiInput);
  }

  circularsView = (circulars) => {
    const { userType } = this.state;
    return (
      <View>
        <NoticeBoard contents={circulars} userType={userType} tabName={HOME_SCREEN_TABS_NAME.CIRCULAR} />
      </View>
    );
  }

  render() {
    const circulars = this.props.circulars;
    return (
      <View style={{ marginVertical: (circulars) ? (!lodash.isEmpty(circulars)) ? SIZES.margin2 : null : SIZES.margin2, }}>
        {(circulars) ? (!lodash.isEmpty(circulars)) ? this.circularsView(circulars) : null : this.circularsView(null)}
      </View >
    );
  }
}

const mapStateToProps = ({ authData, circularHome }) => ({
  user: authData,
  circulars: circularHome.circulars,
});

const mapDispatchToProps = dispatch => ({
  getCircularsAction: apiInput => dispatch(getCircularsRequestActions(apiInput)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Circulars);