import { GET_CIRCULARS_ACTION_CONSTANTS } from '../constants/constants';

const INITIAL_STATE = {
  circulars: null,
}

export default (state = INITIAL_STATE, action) => {
  let currentState = state;

  switch (action.type) {
    case GET_CIRCULARS_ACTION_CONSTANTS.GET_CIRCULARS_REQUEST_ACTION:
      break;

    case GET_CIRCULARS_ACTION_CONSTANTS.GET_CIRCULARS_SUCCESS_ACTION:
      currentState = {
        ...state,
        circulars: action.payload.instituteCirculars,
      }

    default:
      break;
  }

  return currentState;
}