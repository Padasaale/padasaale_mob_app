import { networkHandler } from '../../../../utils/network/NetworkHandler';
import { RESULTS_API_ENDPOINT } from '../constants/apiConstants'
import { USER_TYPES } from '../../../common/Constants/Constants'

export const getResultsApi = async (apiInput) => {
  const { userId, userType } = apiInput;
  try {
    const url = ((() => {
      switch (userType) {
        case USER_TYPES.FACULTY:
          return `${RESULTS_API_ENDPOINT.GET_RESULTS_API_END_POINT}?assignedBy=${userId}`;
        case USER_TYPES.STUDENT:
          return `${RESULTS_API_ENDPOINT.GET_RESULTS_API_END_POINT}?securedTo=${userId}`;
        default:
          break;
      }
    })());
    const getResultsResponse = await networkHandler.get(url);
    return getResultsResponse.data;
  } catch (error) {
    console.log('Get results api error', error);
    return error;
  }
}