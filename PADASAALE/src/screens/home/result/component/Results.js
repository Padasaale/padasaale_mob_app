import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text } from 'react-native'
import lodash from 'lodash'
import { getResultsRequestActions } from '../actions/ResultHomeActions';
import { SIZES } from '../../../../utils/ThemeProvider/ThemeProvider';
import { NoticeBoard } from '../../../common/NoticeBoardCommonComponents/NoticeBoard'
import { HOME_SCREEN_TABS_NAME } from '../../../common/Constants/Constants';

class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // userId: '5f8a7f846e737a4001fe1fce',
      // userType: 'faculty',
      userId: '5f4a2cbd006c3b0aa299e73e',
      userType: 'student'
    }
  }

  componentDidMount() {
    const { userId, userType } = this.state;
    const apiInput = {
      userId: userId,
      userType: userType,
    }
    this.props.getResultsAction(apiInput);
  }

  resultsView = (results) => {
    const { userType } = this.state;
    return (
      <View>
        <NoticeBoard contents={results} userType={userType} tabName={HOME_SCREEN_TABS_NAME.RESULT} />
      </View>
    );
  }

  render() {
    const results = this.props.results;
    return (
      <View style={{ marginVertical: (results) ? (!lodash.isEmpty(results)) ? SIZES.margin2 : null : SIZES.margin2, }}>
        {(results) ? (!lodash.isEmpty(results)) ? this.resultsView(results) : null : this.resultsView(null)}
      </View >
    );
  }
}

const mapStateToProps = ({ authData, resultHome }) => ({
  user: authData,
  results: resultHome.results,
});

const mapDispatchToProps = dispatch => ({
  getResultsAction: apiInput => dispatch(getResultsRequestActions(apiInput)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Results);