import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, Image, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { SIZES, FONTS, THEME } from '../../../../utils/ThemeProvider/ThemeProvider';
import Results from './Results';

class ResultsHome extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerStyle: {
        backgroundColor: THEME.colorPrimary,
      }
    });
  }

  render() {
    return (
      <SafeAreaView style={style.container}>
        <View style={{ flex: 1 }}>
          <Results />
        </View>
        <View style={{ height: 64 }} />
      </SafeAreaView >
    );
  }
}

export default ResultsHome;

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.colorPrimary,
  }
});