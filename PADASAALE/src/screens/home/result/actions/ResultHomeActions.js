import { GET_RESULTS_ACTION_CONSTANTS } from '../constants/constants';

export const getResultsRequestActions = (apiInput) => ({
  type: GET_RESULTS_ACTION_CONSTANTS.GET_RESULTS_REQUEST_ACTION,
  payload: {
    apiInput,
  }
});

export const getResultsSuccessActions = (response) => ({
  type: GET_RESULTS_ACTION_CONSTANTS.GET_RESULTS_SUCCESS_ACTION,
  payload: response,
});