import { GET_RESULTS_ACTION_CONSTANTS } from '../constants/constants';

const INITIAL_STATE = {
  results: null,
}

export default (state = INITIAL_STATE, action) => {
  let currentState = state;

  switch (action.type) {
    case GET_RESULTS_ACTION_CONSTANTS.GET_RESULTS_REQUEST_ACTION:
      break;

    case GET_RESULTS_ACTION_CONSTANTS.GET_RESULTS_SUCCESS_ACTION:
      currentState = {
        ...state,
        results: action.payload.instituteResults,
      }

    default:
      break;
  }

  return currentState;
}