import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import lodash from 'lodash'
import { getActivitiesRequestActions } from '../actions/ActivityHomeActions';
import { SIZES } from '../../../../utils/ThemeProvider/ThemeProvider';
import { NoticeBoard } from '../../../common/NoticeBoardCommonComponents/NoticeBoard'
import { HOME_SCREEN_TABS_NAME } from '../../../common/Constants/Constants';

class Activities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '5f8a7f846e737a4001fe1fce',
      userType: 'faculty',
      // userId: '5f4a2cbd006c3b0aa299e73e',
      // userType: 'student'
    }
  }

  componentDidMount() {
    const { userId, userType } = this.state;
    const apiInput = {
      userId: userId,
      userType: userType,
    }
    this.props.getActivitiesAction(apiInput);
  }

  activitiesView = (activities) => {
    const { userType } = this.state;
    return (
      <View>
        <NoticeBoard contents={activities} userType={userType} tabName={HOME_SCREEN_TABS_NAME.ACTIVITY} />
      </View>
    );
  }

  render() {
    const activities = this.props.activities;
    return (
      <View style={{ marginVertical: (activities) ? (!lodash.isEmpty(activities)) ? SIZES.margin2 : null : SIZES.margin2, }}>
        {(activities) ? (!lodash.isEmpty(activities)) ? this.activitiesView(activities) : null : this.activitiesView(null)}
      </View >
    );
  }
}

const mapStateToProps = ({ authData, activityHome }) => ({
  user: authData,
  activities: activityHome.activities,
});

const mapDispatchToProps = dispatch => ({
  getActivitiesAction: apiInput => dispatch(getActivitiesRequestActions(apiInput)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Activities);