import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native'
import { THEME } from '../../../../utils/ThemeProvider/ThemeProvider';
import Activities from './Activities'

class ActivityHome extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerStyle: {
        backgroundColor: THEME.colorPrimary,
      }
    });
  }

  render() {
    return (
      <SafeAreaView style={style.container}>
        <View style={{ flex: 1 }}>
          <Activities />
        </View>
        <View style={{ height: 64 }} />
      </SafeAreaView >
    );
  }
}

export default ActivityHome;

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME.colorPrimary,
  }
});