import { networkHandler } from '../../../../utils/network/NetworkHandler';
import { ACTIVITIES_API_ENDPOINT } from '../constants/apiConstants'
import { USER_TYPES } from '../../../common/Constants/Constants'

export const getActivitiesApi = async (apiInput) => {
  const { userId, userType } = apiInput;
  try {
    const url = ((() => {
      switch (userType) {
        case USER_TYPES.FACULTY:
          return `users/${userId}/${ACTIVITIES_API_ENDPOINT.GET_ACTIVITIES_API_END_POINT}?assignedBy=true`;
        case USER_TYPES.STUDENT:
          return `users/${userId}/${ACTIVITIES_API_ENDPOINT}`;
        default:
          break;
      }
    })());
    const getActivitiesResponse = await networkHandler.get(url);
    return getActivitiesResponse.data;
  } catch (error) {
    console.log('Get activities api error', error);
    return error;
  }
}