import { GET_ACTIVITIES_ACTION_CONSTANTS } from '../constants/constants';

export const getActivitiesRequestActions = (apiInput) => ({
  type: GET_ACTIVITIES_ACTION_CONSTANTS.GET_ACTIVITIES_REQUEST_ACTION,
  payload: {
    apiInput,
  }
});

export const getActivitiesSuccessActions = (response) => ({
  type: GET_ACTIVITIES_ACTION_CONSTANTS.GET_ACTIVITIES_SUCCESS_ACTION,
  payload: response,
});