import { GET_ACTIVITIES_ACTION_CONSTANTS } from '../constants/constants';

const INITIAL_STATE = {
  activities: null,
}

export default (state = INITIAL_STATE, action) => {
  let currentState = state;

  switch (action.type) {
    case GET_ACTIVITIES_ACTION_CONSTANTS.GET_ACTIVITIES_REQUEST_ACTION:
      break;

    case GET_ACTIVITIES_ACTION_CONSTANTS.GET_ACTIVITIES_SUCCESS_ACTION:
      currentState = {
        ...state,
        activities: action.payload.instituteActivities,
      }

    default:
      break;
  }

  return currentState;
}