import { takeEvery, call, put } from 'redux-saga/effects';
import { GET_ACTIVITIES_ACTION_CONSTANTS } from '../constants/constants';
import { getActivitiesApi } from '../api/ActivityHomeApis';
import { getActivitiesSuccessActions } from '../actions/ActivityHomeActions';

export default [
  takeEvery(GET_ACTIVITIES_ACTION_CONSTANTS.GET_ACTIVITIES_REQUEST_ACTION, getActivitiesFunSaga),
];

function* getActivitiesFunSaga(action) {
  const { apiInput } = action.payload;
  const getActivitiesApiResponse = yield call(getActivitiesApi, apiInput);
  if (getActivitiesApiResponse && getActivitiesApiResponse.status === 'SUCCESS') {
    yield put(getActivitiesSuccessActions(getActivitiesApiResponse));
  }
}