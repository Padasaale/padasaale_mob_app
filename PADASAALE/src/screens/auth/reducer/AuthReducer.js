import { SIGNIN_ACTION_CONSTANTS } from '../constants/constants';
import { SHARED_PREFERENCE_ACTION_CONSTANTS } from '../../../common/constants/AppConstants';

const INITIAL_STATE = {
  isSignedIn: false,
  user: null,
}

export default (state = INITIAL_STATE, action) => {
  let currentState = state;
  switch (action.type) {
    case SIGNIN_ACTION_CONSTANTS.SIGNIN_SUCCESS_ACTION:
      currentState = {
        ...state,
        isSignedIn: true,
        user: action.payload,
      }
      break;
    case SHARED_PREFERENCE_ACTION_CONSTANTS.UPDATE_USER_DATA:
      currentState = {
        ...state,
        isSignedIn: action.payload.USER_SIGNED_IN,
        user: action.payload.USER_DETAILS
      };
      break;
    case SHARED_PREFERENCE_ACTION_CONSTANTS.RESET_ALL_REDUCERS_STATE:
      currentState = INITIAL_STATE;
      break;
    default: break;
  }

  return currentState;
}