import { SHARED_PREFERENCE } from '../../../utils/SharedPreferences';
import { SHARED_PREFERENCE_KEYS } from '../../../common/constants/AppConstants';

export const saveSignInUserDataInDeviceStore = async signInData => {
  const { user } = signInData;
  try {
    await SHARED_PREFERENCE.accessor.put(SHARED_PREFERENCE_KEYS.USER_SIGNED_IN, 'true');
    await SHARED_PREFERENCE.accessor.put(SHARED_PREFERENCE_KEYS.USER_ID, user.userId.toString());
    await SHARED_PREFERENCE.accessor.put(SHARED_PREFERENCE_KEYS.USER_DETAILS, JSON.stringify(signInData));
    return;
  } catch (exception) {
    console.log('---> Unable to save data in preference', exception);
  }
}

export const clearLoginUserDataInDeviceStore = async () => {
  try {
    await SHARED_PREFERENCE.accessor.removeAll();
    return;
  } catch (exception) {
    console.log('error while removing data from preference');
    return;
  }
}