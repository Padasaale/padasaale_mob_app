import { networkHandler } from '../../../utils/network/NetworkHandler';
import { AUTH_API_CONSTANTS } from '../constants/apiConstants';

export const signInApi = async (apiInput) => {
  const { userId, userPassword } = apiInput;
  const input = {
    userName: userId,
    password: userPassword,
  }
  try {
    const url = `${AUTH_API_CONSTANTS.SIGNIN_API_END_POINT}`;
    const signInResponse = await networkHandler.post(url, input);
    return signInResponse.data;
  } catch (error) {
    console.log('SignIn api error', error);
    return error;
  }
}

export const signUpApi = async (apiInput) => {
  try {
    const url = `${AUTH_API_CONSTANTS.SIGNUP_API_END_POINT}`;
    const signUpResponse = await networkHandler.post(url, apiInput);
    return signUpResponse.data;
  } catch (error) {
    console.log('SignUp api error', error);
    return error;
  }
}