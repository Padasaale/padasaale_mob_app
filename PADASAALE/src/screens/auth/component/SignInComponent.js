import React, { Component } from 'react';
import { connect } from 'react-redux'
import { SafeAreaView, View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { FONTS, SIZES, THEME } from '../../../utils/ThemeProvider/ThemeProvider';
import { HelperText } from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons'
import CheckBox from '@react-native-community/checkbox'
import { signInRequestAction } from '../actions/AuthActions'
import { DTAlert } from '../../common/DTAlert/DTAlert'
import { DTSpinner } from '../../common/DTSpinner/DTSpinner';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      userPassword: '',
      isSignInFailed: false,
      securePasswordEntry: true,
      isSubscribed: true,
      isLoading: false,
    };
  }

  onUserNameCangeHandler = userId => {
    this.setState({
      ...this.state,
      userId,
    });
  }

  onPasswordCangeHandler = userPassword => {
    this.setState({
      ...this.state,
      userPassword,
    });
  }

  onSecurePasswordClickHandler = () => {
    this.setState({
      ...this.state,
      securePasswordEntry: !this.state.securePasswordEntry,
    });
  }

  onSignInFailed = () => {
    console.log('SignIn failed');
    this.setState({
      ...this.state,
      isSignInFailed: true,
      isLoading: false,
    });
  }

  onSignInClickHandler = () => {
    const { userId, userPassword } = this.state;
    const apiInput = { userId, userPassword };
    this.setState({
      ...this.state,
      isLoading: true,
    });
    this.props.signInAction(apiInput, this.onSignInFailed);
  }

  onLoginFailedConfirmAlertClickHandler = () => {
    this.setState({
      ...this.state,
      isSignInFailed: false,
    });
  }

  onSubscribeClickHandler = () => {
    this.setState({
      ...this.state,
      isSubscribed: !this.state.isSubscribed,
    });
  }

  onResetPasswordClickHandler = () => {
    this.props.navigation.navigate('ResetPassword');
  }

  onSignUpClickHandler = () => {
    this.props.navigation.navigate('EmailRequest');
  }

  loginFailAlertDialogView = () => {
    const { isSignInFailed } = this.state;
    return (
      <DTAlert
        onConfirm={this.onLoginFailedConfirmAlertClickHandler}
        headerMessage={'Login failed'}
        message={'Invalid username or passcode'}
        visible={isSignInFailed}
      />
    );
  }

  wellComeTextView = () => (
    <View>
      <Text style={{ ...FONTS.largeTitle2_bold, color: THEME.textPrimaryColor }}>WELCOME BACK</Text>
    </View>
  );

  userNameInputView = () => {
    return (
      <View>
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>Enter your credentials to access your PADASAALE account</Text>
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="Email id (Eg: jnanesh123@gmail.com)"
            onChangeText={this.onUserNameCangeHandler}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
      </View>
    );
  }

  passwordInputView = () => {
    const { securePasswordEntry } = this.state;
    return (
      <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <TextInput
          placeholder="Enter pass code"
          onChangeText={this.onPasswordCangeHandler}
          style={{ flex: 1, color: THEME.textPrimaryColor }}
          ref={this.passwordField}
          secureTextEntry={securePasswordEntry ? true : false}
        />
        <Icon
          name={securePasswordEntry ? 'eye-off' : 'eye'}
          size={20}
          color={THEME.textPrimaryColor}
          style={{ paddingHorizontal: SIZES.padding3 }}
          onPress={this.onSecurePasswordClickHandler} />
      </View>
    )
  }

  subscribeAndSignInButtonView = () => {
    const { isSubscribed } = this.state;
    return (
      <View style={style.resetAndSignInViewContainer}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <CheckBox
            value={isSubscribed}
            onValueChange={this.onSubscribeClickHandler}
            tintColors={{ true: THEME.colorAccentDark, false: THEME.colorBlack }}
          />
          <Text style={{ ...FONTS.h4, color: THEME.colorAccentDark }}>Check Box to subscribe to our newsletters</Text>
        </View>
        <View style={{ justifyContent: "center" }}>
          <TouchableOpacity onPress={this.onSignInClickHandler}>
            <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2), borderRadius: (SIZES.padding2 * 4), ...SIZES.card }}>
              <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccentDark }}>SIGN IN</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  resetAndSignInButtonView = () => {
    return (
      <View style={style.resetAndSignInViewContainer}>
        <View style={{ justifyContent: "center" }}>
          <TouchableOpacity onPress={this.onResetPasswordClickHandler}>
            <View style={{ paddingVertical: SIZES.padding2 }}>
              <Text style={{ ...FONTS.h4, color: THEME.colorAccent2 }}>Forgot Passcode?</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ justifyContent: "center" }}>
          <TouchableOpacity onPress={this.onSignInClickHandler}>
            <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2), borderRadius: (SIZES.padding2 * 4), ...SIZES.card }}>
              <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent2 }}>SIGN IN</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  signUpView = () => {
    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', marginVertical: SIZES.margin3 }}>
          <Text style={{ ...FONTS.h3, color: THEME.textPrimaryColor }}>Don't have an account ? </Text>
          <TouchableOpacity onPress={this.onSignUpClickHandler}>
            <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent }}>Sign up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  appImageView = () => {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text style={{ ...FONTS.largeTitle_bold, color: THEME.textPrimaryColor }}>PADASAALE</Text>
      </View>
    );
  }

  signInView = () => {
    const { isLoading } = this.state;
    return (
      <>
        {isLoading ? (<DTSpinner size='large' />) :
          (
            <View style={{ flex: 1 }}>
              {this.loginFailAlertDialogView()}
              <ScrollView contentContainerStyle={style.scrollViewContainer}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  {this.appImageView()}
                </View>
                <View style={{ flex: 4, paddingHorizontal: SIZES.padding3, paddingTop: SIZES.padding3 * 5, ...SIZES.card, borderTopLeftRadius: SIZES.borderRadius2 * 3, borderTopRightRadius: SIZES.borderRadius2 * 3, }}>
                  <View style={{ flex: 1, }}>
                    {this.wellComeTextView()}
                    {this.userNameInputView()}
                    {this.passwordInputView()}
                    {this.resetAndSignInButtonView()}
                  </View>
                  {this.signUpView()}
                </View>
              </ScrollView>
            </View>
          )
        }
      </>
    );
  };

  render() {
    return (
      <SafeAreaView style={style.container}>
        {this.signInView()}
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  signInAction: (apiInput, callback) => dispatch(signInRequestAction(apiInput, callback))
});

export default connect(null, mapDispatchToProps)(SignIn);

const style = StyleSheet.create({
  container: {
    backgroundColor: THEME.colorPrimary,
    flex: 1,
    justifyContent: 'center',
  },

  scrollViewContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },

  resetAndSignInViewContainer: {
    marginTop: (SIZES.margin3 * 2),
    flexDirection: "row",
    justifyContent: "space-between"
  }
});