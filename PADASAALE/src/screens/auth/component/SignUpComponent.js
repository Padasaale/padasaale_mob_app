import React, { Component } from 'react'
import { connect } from 'react-redux'
import { SafeAreaView, View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity, Image } from 'react-native'
import { signUpRequestAction } from '../actions/AuthActions';
import { DTSpinner } from '../../common/DTSpinner/DTSpinner';
import { DTAlert } from '../../common/DTAlert/DTAlert';
import { HelperText } from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons';
import { THEME, SIZES, FONTS } from '../../../utils/ThemeProvider/ThemeProvider';

class SignUpComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmailId: props.route.params.UserEmailId ? props.route.params.UserEmailId : null,
      userType: props.route.params.userType ? props.route.params.userType : null,
      isSubscribed: props.route.params.subscribed ? props.route.params.subscribed : null,
      userPhoneNo: null,
      userFirstName: null,
      userMiddleName: null,
      userLastName: null,
      userPassword: null,
      confirmPassword: false,
      isSignUpFailed: false,
      securePasswordEntry: true,
      isLoading: false,
      isValidPassword: false,
    };
    this.passwordField = React.createRef();
  }

  onResetPasswordClickHandler = () => {
    this.props.navigation.navigate('ResetPassword');
  }

  onSignUpClickHandler = () => {
    this.props.navigation.navigate('EmailRequest');
  }
  onLoginFailedConfirmAlertClickHandler = () => {
    this.setState({
      ...this.state,
      isSignUpFailed: false,
    });
  }

  onSecurePasswordClickHandler = () => {
    this.setState({
      ...this.state,
      securePasswordEntry: !this.state.securePasswordEntry,
    });
  }

  onPhoneNoChangeHandler = (userPhoneNo) => {
    this.setState({
      ...this.state,
      userPhoneNo: userPhoneNo,
    });
  }

  onFirstNameChangeHandler = (firstName) => {
    this.setState({
      ...this.state,
      userFirstName: firstName,
    });
  }

  onMiddeleNameChangeHandler = (middleName) => {
    this.setState({
      ...this.state,
      userMiddleName: middleName,
    });
  }

  onLastNameChangeHandler = (lastName) => {
    this.setState({
      ...this.state,
      userLastName: lastName,
    });
  }

  onPasswordCangeHandler = userPassword => {
    const regularExpression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
    const passwordNumericValue = (regularExpression).test(userPassword);
    this.setState({
      ...this.state,
      userPassword,
      isValidPassword: passwordNumericValue,
    });
  }

  onReEnterdPasswordChangeHandler = (confirmPassword) => {
    const { userPassword } = this.state;
    if (confirmPassword === userPassword) {
      this.setState({
        ...this.state,
        confirmPassword: true,
      });
    } else {
      this.setState({
        ...this.state,
        confirmPassword: false,
      });
    }
  }

  onSignInFailed = () => {
    console.log('SignIn failed');
    this.setState({
      ...this.state,
      isSignUpFailed: true,
      isLoading: false,
    });
  }

  onSignInClickHandler = () => {
    const { userEmailId, userPassword } = this.state;
    const apiInput = { userEmailId, userPassword };
    this.setState({
      ...this.state,
      isLoading: true,
    });
    this.props.signInAction(apiInput, this.onSignInFailed);
  }


  onBackPressHandler = () => {
    this.props.navigation.goBack();
  }

  onSignUpSuccess = () => {
    this.setState({
      ...this.state,
      isLoading: false,
    });
    this.props.navigation.navigate('SignIn');
  }

  onSignUpFailed = () => {
    this.setState({
      ...this.state,
      isLoading: false,
      isSignUpFailed: true,
    });
  }

  onSubmitClickHandler = () => {
    const { userEmailId, userPhoneNo, userType, userFirstName, userMiddleName, userLastName, userPassword } = this.state;
    const apiInput = {
      firstName: userFirstName,
      middleName: userMiddleName,
      lastName: userLastName,
      emailId: userEmailId,
      phoneNumber: userPhoneNo,
      userType: userType,
      password: userPassword,
    }
    this.setState({
      ...this.state,
      isLoading: true,
    });
    this.props.signUpAction(apiInput, this.onSignUpSuccess, this.onSignUpFailed);
  }

  wellComeTextView = () => {
    return (
      <View>
        <View>
          <Text style={{ ...FONTS.largeTitle2_bold, color: THEME.textPrimaryColor }}>CREATING</Text>
        </View>
        <View>
          <Text style={{ ...FONTS.largeTitle2_bold, color: THEME.textPrimaryColor }}>YOUR ACCOUNT</Text>
        </View>
      </View>
    );
  }

  mobileNoInputView = () => {
    return (
      <View style={{ marginTop: SIZES.margin3 }}>
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>To keep us connected we need your mobile number</Text>
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="Mobile number (Eg: 9742171000)"
            onChangeText={this.onPhoneNoChangeHandler}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
      </View>
    );
  }

  userNameInputView = () => {
    return (
      <View style={{ marginTop: SIZES.margin3 }}>
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>To keep us connected we need your mobile number</Text>
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="First name"
            onChangeText={this.onFirstNameChangeHandler}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="Middle name"
            onChangeText={this.onMiddeleNameChangeHandler}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="Last name"
            onChangeText={this.onLastNameChangeHandler}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
      </View>
    )
  }

  passwordInputView = () => {
    const { confirmPassword, securePasswordEntry } = this.state;
    return (
      <View style={{ marginTop: SIZES.margin3 }}>
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>Set passcode to secure your account</Text>
        </View>
        <View>
          <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <TextInput
              placeholder="Enter pass code"
              onChangeText={this.onPasswordCangeHandler}
              style={{ flex: 1, color: THEME.textPrimaryColor }}
              ref={this.passwordField}
              secureTextEntry={securePasswordEntry ? true : false}
            />
            <Icon
              name={securePasswordEntry ? 'eye-off' : 'eye'}
              size={20}
              color={THEME.textPrimaryColor}
              style={{ paddingHorizontal: SIZES.padding3 }}
              onPress={this.onSecurePasswordClickHandler} />
          </View>
          <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <TextInput
              placeholder="Re enter pass code"
              onChangeText={this.onReEnterdPasswordChangeHandler}
              style={{ flex: 1, color: THEME.textPrimaryColor }}
              ref={this.passwordField}
              secureTextEntry={securePasswordEntry ? true : false}
            />
            <Icon
              name={confirmPassword ? 'checkmark-circle-outline' : null}
              size={20}
              color={THEME.textPrimaryColor}
              style={{ paddingHorizontal: SIZES.padding3 }} />
          </View>
        </View>
      </View>
    );
  }

  backAndSubmitButtonView = () => {
    return (
      <View style={style.resetAndsignUpViewContainer}>
        <View style={{ justifyContent: "center", flexDirection: 'row' }}>
          <View>
            <TouchableOpacity onPress={() => this.onBackPressHandler()}>
              <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2) }}>
                <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent2 }}>BACK</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => this.onSubmitClickHandler()}>
              <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2), borderRadius: (SIZES.padding2 * 4), ...SIZES.card }}>
                <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent2 }}>SUBMIT</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  appImageView = () => {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text style={{ ...FONTS.largeTitle_bold, color: THEME.textPrimaryColor }}>PADASAALE</Text>
      </View>
    )
  }

  signUpFailAlertDialogView = () => {
    const { isSignUpFailed } = this.state;
    return (
      <DTAlert
        onConfirm={this.onLoginFailedConfirmAlertClickHandler}
        headerMessage={'Sign up failed'}
        message={'Something went wrong please try again later'}
        visible={isSignUpFailed}
      />
    );
  }

  signUpView = () => {
    const { isLoading } = this.state;
    return (
      <>
        {isLoading ? (<DTSpinner size='large' />) :
          (
            <View style={{ flex: 1 }}>
              {this.signUpFailAlertDialogView()}
              <View style={{ flex: 1, justifyContent: 'center' }}>
                {this.appImageView()}
              </View>
              <View style={{ flex: 4, paddingHorizontal: SIZES.padding3, paddingTop: SIZES.padding3, ...SIZES.card, borderTopLeftRadius: SIZES.borderRadius2 * 3, borderTopRightRadius: SIZES.borderRadius2 * 3, }}>
                <ScrollView contentContainerStyle={style.scrollViewContainer}
                  showsVerticalScrollIndicator={false}>
                  <View style={{ flex: 1, marginTop: SIZES.margin3 * 4, }}>
                    {this.wellComeTextView()}
                    {this.mobileNoInputView()}
                    {this.userNameInputView()}
                    {this.passwordInputView()}
                    {this.backAndSubmitButtonView()}
                  </View>
                </ScrollView>
              </View>
            </View>
          )
        }
      </>
    );
  }

  render() {
    return (
      <SafeAreaView style={style.container}>
        {this.signUpView()}
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  signUpAction: (apiInput, successCallback, failureCallback) => dispatch(signUpRequestAction(apiInput, successCallback, failureCallback)),
});

export default connect(null, mapDispatchToProps)(SignUpComponent);

const style = StyleSheet.create({
  container: {
    backgroundColor: THEME.colorPrimary,
    flex: 1,
    justifyContent: 'center',
  },

  scrollViewContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },

  resetAndsignUpViewContainer: {
    marginVertical: (SIZES.margin3 * 2),
    flexDirection: "row",
    justifyContent: 'flex-end'
  }
})