import React, { useState } from 'react'
import { connect } from 'react-redux'
import { SafeAreaView, View, Text, TextInput, Image, ScrollView, TouchableOpacity, StyleSheet, Alert, FlatList } from 'react-native'
import { getDeviceHeight, getDeviceWidth } from '../../../utils/CommonUtils'
import CheckBox from '@react-native-community/checkbox'
import DTIcon from '../../../assets/icons/IconProvider'
// import { DTAlert } from '../../common/DTAlert/DTAlert'
import { FONTS, SIZES, THEME } from '../../../utils/ThemeProvider/ThemeProvider';

const EmailRequestComponent = props => {
  const [isSubscribed, setSubscription] = useState(true);
  const [userEmailId, onChangeUserEmailId] = useState(null);
  const [isEmailInvalid, setEmailInvalid] = useState(false);
  const [userTypes, setUserTypes] = useState([{ id: 0, type: 'faculty' }, { id: 1, type: 'student' }, { id: 2, type: 'parent' }, { id: 3, type: 'guardian' },])
  const [selectedUserType, setSelectedUserType] = useState(userTypes[0].type);

  const verifyEmailId = response => {
    const { user } = response;
    if (user === 'no match found') {
      const data = {
        UserEmailId: userEmailId,
        subscribed: isSubscribed,
      };
      props.navigation.navigate('SignUp', data);
    } else {
      Alert.alert(
        "You have an account already",
        null,
        [
          { text: "OK", onPress: () => props.navigation.goBack() }
        ],
        { cancelable: false }
      );
    }
  }

  const onInvalidEmailconfirm = () => {
    setEmailInvalid(false);
  }

  const onNextClickHandler = () => {
    if (validator.isEmail(userEmailId)) {
      const apiInput = {
        userEmailId,
      }
      props.getUserByEmailIdAction(apiInput, verifyEmailId);
    }
    else {
      setEmailInvalid(true);
    }
  }

  // const invalidEmailAlertDialogView = () => {
  //     return (
  //         <DTAlert
  //             onConfirm={onInvalidEmailconfirm}
  //             headerMessage={'Invalid Email'}
  //             message={'Please try again.'}
  //             visible={isEmailInvalid}
  //         />
  //     );
  // }

  const onUserTypeSelectionHandler = (item) => {
    const { type } = item;
    setSelectedUserType(type);
  }

  const onBackPressClickHandler = () => {
    props.navigation.goBack();
  }

  const onVerifyEmailClickHandler = () => {
    const data = {
      UserEmailId: userEmailId,
      subscribed: isSubscribed,
      userType: selectedUserType,
    };
    props.navigation.navigate('SignUp', data);
  }

  const appImageView = () => {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text style={{ ...FONTS.largeTitle_bold, color: THEME.textPrimaryColor }}>PADASAALE</Text>
      </View>
    )
  }

  const wellComeTextView = () => (
    <View>
      <Text style={{ ...FONTS.largeTitle2_bold, color: THEME.textPrimaryColor }}>NEW USER</Text>
    </View>
  );

  const userTypeSelectionView = () => {
    const selectedType = selectedUserType;
    return (
      <View >
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>Select who you are!</Text>
        </View>
        <View>
          <FlatList
            data={userTypes}
            horizontal
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              const type = item.type;
              return (
                <View style={{ width: ((getDeviceWidth() - SIZES.padding3 * 2) / 4), paddingHorizontal: SIZES.padding, marginVertical: SIZES.margin3 }}>
                  <TouchableOpacity onPress={() => onUserTypeSelectionHandler(item)}>
                    <View style={{ borderRadius: ((SIZES.margin3 * 8) / 2), justifyContent: 'center', alignItems: 'center', borderWidth: (type === selectedType) ? null : SIZES.borderWidth, backgroundColor: (type === selectedType) ? THEME.colorAccent2 : THEME.colorPrimary }}>
                      <Text style={{ textTransform: 'uppercase', ...FONTS.h4, color: (type === selectedType) ? THEME.textPrimaryDarkColor : THEME.textPrimaryColor, marginVertical: SIZES.margin2 }}>{type}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
            showsHorizontalScrollIndicator={false} />
        </View>
      </View>
    );
  }

  const userEmailInputView = () => {
    return (
      <View style={{ marginTop: SIZES.margin3 }}>
        <View>
          <Text style={{ ...FONTS.h1, color: THEME.textPrimaryColor }}>Provide your email id</Text>
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: THEME.textPrimaryColor }}>
          <TextInput
            placeholder="Email id (Eg: jnanesh123@gmail.com)"
            onChangeText={userEmail => onChangeUserEmailId(userEmail)}
            style={{ color: THEME.textPrimaryColor }}
          />
        </View>
      </View>
    );
  }

  const subScribeAndNextButtonView = () => {
    return (
      <View style={style.subScribeAndNextButtonViewContainer}>
        <View style={{ justifyContent: "center", flexDirection: 'row' }}>
          <View>
            <CheckBox
              value={isSubscribed}
              onValueChange={setSubscription}
              tintColors={{ true: THEME.colorAccent2, false: THEME.colorPrimaryDark }}
            />
          </View>
          <View style={{ paddingVertical: SIZES.padding2, justifyContent: 'center' }}>
            <Text style={{ ...FONTS.h4, color: THEME.colorAccent2 }}>Subscribe to our newsletters</Text>
          </View>

        </View>
        <View style={{ justifyContent: "center", flexDirection: 'row' }}>
          <View>
            <TouchableOpacity onPress={() => onBackPressClickHandler()}>
              <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2) }}>
                <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent2 }}>BACK</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => onVerifyEmailClickHandler()}>
              <View style={{ backgroundColor: THEME.colorPrimary, paddingVertical: SIZES.padding2, paddingHorizontal: (SIZES.padding3 * 2), borderRadius: (SIZES.padding2 * 4), ...SIZES.card }}>
                <Text style={{ ...FONTS.h3_bold, color: THEME.colorAccent2 }}>SEND LINK</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  const emailRequestView = () => {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.scrollViewContainer}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            {appImageView()}
          </View>
          <View style={{ flex: 4, paddingHorizontal: SIZES.padding3, paddingTop: SIZES.padding3 * 5, ...SIZES.card, borderTopLeftRadius: SIZES.borderRadius2 * 3, borderTopRightRadius: SIZES.borderRadius2 * 3, }}>
            <View style={{ flex: 1 }}>
              {wellComeTextView()}
              {userTypeSelectionView()}
              {userEmailInputView()}
              {subScribeAndNextButtonView()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

  return (
    <SafeAreaView style={style.container}>
      {emailRequestView()}
    </SafeAreaView>
    // <SafeAreaView style={style.conatainer}>
    //     {/* {invalidEmailAlertDialogView()} */}
    //     <ScrollView contentContainerStyle={style.scrollViewContainer}
    //         showsVerticalScrollIndicator={false}>
    //         <View style={{ height: getDeviceHeight(), padding: 8 }}>
    //             <View style={{ flex: 7, justifyContent: 'center' }}>
    //                 {loginWellcomeView()}
    //                 {emailView()}
    //                 {subscribeAndSubmitView()}
    //             </View>
    //             <View style={{ flex: 1 }}>
    //                 {appImageView()}
    //             </View>
    //         </View>
    //     </ScrollView>
    // </SafeAreaView>
  );
}

const mapStateToProps = state => ({
  // user: state.signIn.user.user,
  // cart: state.eStoreCart.cart,
});

const mapDispatchToProps = dispatch => ({
  getUserByEmailIdAction: (apiInput, callback) => dispatch(getUserByEmailIdRequestAction(apiInput, callback)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailRequestComponent);

const style = StyleSheet.create({
  container: {
    backgroundColor: THEME.colorPrimary,
    flex: 1,
    justifyContent: 'center',
  },
  scrollViewContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  subScribeAndNextButtonViewContainer: {
    marginTop: (SIZES.margin3 * 2),
    flexDirection: "row",
    justifyContent: "space-between"
  },
})


// import React, {useState} from 'react'
// import {connect} from 'react-redux'
// import {SafeAreaView, View, Text, TextInput, Image, ScrollView, TouchableOpacity, StyleSheet, Alert} from 'react-native'
// import {getDeviceHeight} from '../../../utils/CommonUtils'
// import CheckBox from '@react-native-community/checkbox'
// import DTIcon from '../../../assets/icons/IconProvider'
// import AmAptLogo from '../../../assets/images/am_apt_logo.png'
// import {getUserByEmailIdRequestAction} from '../actions/SignUpAction'
// import validator from 'validator'
// // import {DTAlert} from '../../common/DTAlert/DTAlert'

// const EmailRequestComponent = props => {
//     const [isSubscribed, setSubscription] = useState(false);
//     const [userEmailId, onChangeUserEmailId] = useState('');
//     const [isEmailInvalid, setEmailInvalid] = useState(false);

//     const verifyEmailId = response => {
//         const { user } = response;
//         if (user === 'no match found') {
//             const data = {
//                 UserEmailId: userEmailId,
//                 subscribed: isSubscribed,
//             };
//             props.navigation.navigate('SignUp', data);
//         } else {
//             Alert.alert(
//                 "You have an account already",
//                 null,
//                 [
//                     { text: "OK", onPress: () => props.navigation.goBack() }
//                 ],
//                 { cancelable: false }
//             );
//         }
//     }

//     const onInvalidEmailconfirm = () => {
//         setEmailInvalid(false);
//     }

//     const onNextClickHandler = () => {
//         if (validator.isEmail(userEmailId)) {
//             const apiInput = {
//                 userEmailId,
//             }
//             props.getUserByEmailIdAction(apiInput, verifyEmailId);
//         }
//         else {
//             setEmailInvalid(true);
//         }
//     }

//     const loginWellcomeView = () => {
//         return (
//             <View>
//                 <View>
//                     <Text style={style.welocomeText}>WELCOME</Text>
//                 </View>
//                 <View>
//                     <Text style={{ fontSize: 20 }}>To create your APT account, we need your email ID</Text>
//                 </View>
//             </View>
//         )
//     }

//     const emailView = () => {
//         return (
//             <View>
//                 <TextInput placeholder="Email id (Eg: jnanesh123@gmail.com)"
//                     onChangeText={userEmail => onChangeUserEmailId(userEmail)}
//                     style={{ borderBottomWidth: 0.5 }}
//                 />
//             </View>
//         );
//     }

//     const subscribeAndSubmitView = () => {
//         return (
//             <View style={style.subscribeAndSubmitViewContainer}>
//                 <View style={{ flexDirection: "row", alignItems: "center" }}>
//                     <CheckBox
//                         value={isSubscribed}
//                         onValueChange={setSubscription}
//                         tintColors={{ true: '#ff5722', false: 'black' }}
//                     />
//                     <Text style={{ fontSize: 12, color: "#ff5722" }}>Check Box to subscribe to our newsletters</Text>
//                 </View>
//                 <View style={{ justifyContent: "center" }}>
//                     <TouchableOpacity onPress={onNextClickHandler}>
//                         <DTIcon
//                             size={40}
//                             name='apt_forward' />
//                     </TouchableOpacity>
//                 </View>
//             </View>
//         );
//     }
//     const appImageView = () => {
//         return (
//             <View style={{ alignItems: 'center' }}>
//                 <Image
//                     style={{
//                         width: 150,
//                         height: 50
//                     }}
//                     resizeMode='contain'
//                     source={AmAptLogo} />
//             </View>
//         )
//     }

//     // const invalidEmailAlertDialogView = () => {
//     //     return (
//     //         <DTAlert
//     //             onConfirm={onInvalidEmailconfirm}
//     //             headerMessage={'Invalid Email'}
//     //             message={'Please try again.'}
//     //             visible={isEmailInvalid}
//     //         />
//     //     );
//     // }

//     return (
//         <SafeAreaView style={style.conatainer}>
//             {/* {invalidEmailAlertDialogView()} */}
//             <ScrollView contentContainerStyle={style.scrollViewContainer}
//                 showsVerticalScrollIndicator={false}>
//                 <View style={{ height: getDeviceHeight(), padding: 8 }}>
//                     <View style={{ flex: 7, justifyContent: 'center' }}>
//                         {loginWellcomeView()}
//                         {emailView()}
//                         {subscribeAndSubmitView()}
//                     </View>
//                     <View style={{ flex: 1 }}>
//                         {appImageView()}
//                     </View>
//                 </View>
//             </ScrollView>
//         </SafeAreaView>
//     );
// }

// const mapStateToProps = state => ({
//     // user: state.signIn.user.user,
//     // cart: state.eStoreCart.cart,
// });

// const mapDispatchToProps = dispatch => ({
//     getUserByEmailIdAction: (apiInput, callback) => dispatch(getUserByEmailIdRequestAction(apiInput, callback)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(EmailRequestComponent);

// const style = StyleSheet.create({
//     conatainer: {
//         flex: 1,
//     },
//     scrollViewContainer: {
//         flexGrow: 1,
//         justifyContent: 'center'
//     },
//     welocomeText: {
//         fontSize: 42
//     },
//     subscribeAndSubmitViewContainer: {
//         marginTop: 25,
//         flexDirection: "row",
//         justifyContent: "space-between"
//     }
// })