import {
  SIGNIN_ACTION_CONSTANTS,
  SIGNUP_ACTION_CONSTANTS,
} from '../constants/constants';

export const signInRequestAction = (apiInput, callback) => ({
  type: SIGNIN_ACTION_CONSTANTS.SIGNIN_REQUEST_ACTION,
  payload: {
    apiInput,
    callback,
  }
});

export const signInSuccessAction = (signInResponse) => ({
  type: SIGNIN_ACTION_CONSTANTS.SIGNIN_SUCCESS_ACTION,
  payload: signInResponse,
});

export const signInFailureAction = () => ({
  type: SIGNIN_ACTION_CONSTANTS.SIGNIN_FAILURE_ACTION,
});

export const signUpRequestAction = (apiInput, successCallback, failureCallback) => ({
  type: SIGNUP_ACTION_CONSTANTS.SIGNUP_REQUEST_ACTION,
  payload: {
    apiInput,
    successCallback,
    failureCallback,
  }
});
