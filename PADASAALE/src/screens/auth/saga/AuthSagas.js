import { takeEvery, call, put } from 'redux-saga/effects';
import {
  SIGNIN_ACTION_CONSTANTS,
  SIGNUP_ACTION_CONSTANTS,
} from '../constants/constants';
import { signInSuccessAction } from '../actions/AuthActions';
import {
  signInApi,
  signUpApi
} from '../api/AuthApi'
import { saveSignInUserDataInDeviceStore } from '../utils/SignInUtils'

export default [
  takeEvery(SIGNIN_ACTION_CONSTANTS.SIGNIN_REQUEST_ACTION, signInFunSaga),
  takeEvery(SIGNUP_ACTION_CONSTANTS.SIGNUP_REQUEST_ACTION, signUpFunSaga),
];

function* signInFunSaga(action) {
  const { apiInput, callback } = action.payload;
  const signiApiResponse = yield call(signInApi, apiInput);
  if (signiApiResponse && signiApiResponse.status === 'SUCCESS') {
    yield put(signInSuccessAction(signiApiResponse));
    yield call(saveSignInUserDataInDeviceStore, signiApiResponse);
  } else {
    callback();
  }
}

function* signUpFunSaga(action) {
  const { apiInput, failureCallback, successCallback } = action.payload;
  const signUpApiResponse = yield call(signUpApi, apiInput);
  if (signUpApiResponse && signUpApiResponse.status === 'SUCCESS') {
    successCallback();
  } else {
    failureCallback();
  }
}