import React from 'react'
import { View, StyleSheet, Text, Modal, TouchableOpacity } from 'react-native'
import { SIZES, THEME, FONTS } from '../../../utils/ThemeProvider/ThemeProvider';
import { getDeviceHalfWidth } from '../../../utils/CommonUtils'

export const DTAlert = (props) => {
  const { onConfirm, headerMessage, message, visible } = props;
  return (
    <Modal
      animationType='fade'
      transparent={true}
      visible={visible}>
      <View style={style.modalContainer}>
        <View style={style.modalView}>
          <View style={style.modalHeaderTextContainer}>
            <Text style={style.modalHederText}>{headerMessage}</Text>
          </View>
          <View style={style.modalMessageTextContainer}>
            <Text style={style.modalMessageText}>{message}</Text>
          </View>
          <TouchableOpacity onPress={() => onConfirm()}>
            <View style={style.modalConfirmTextContainer}>
              <Text style={{ ...FONTS.h4_bold, margin: SIZES.margin2, color: THEME.colorAccent }}>Ok</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

const style = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0,0,0,0.2)'
  },
  modalView: {
    backgroundColor: "white",
    width: (getDeviceHalfWidth() + (getDeviceHalfWidth() / 3)),
    borderRadius: SIZES.borderRadius2,
    padding: SIZES.padding3,
    alignItems: "center",
  },
  modalHeaderTextContainer: {
    marginVertical: SIZES.margin2,
  },
  modalMessageTextContainer: {
    marginVertical: SIZES.margin2,
  },
  modalConfirmTextContainer: {
    marginTop: SIZES.margin,
  },
  modalHederText: {
    ...FONTS.h3_bold,
  },
  modalMessageText: {
    ...FONTS.h4,
  }
});