import React from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'

export const DTSpinner = (props) => {
  const { size } = props;
  return (
    <View style={style.container}>
      <ActivityIndicator size={size} color='#FF893E' />
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  }
});