export const HOME_SCREEN_TABS_NAME = {
  ACTIVITY: 'ACTIVITY',
  CIRCULAR: 'CIRCULAR',
  RESULT: 'RESULT'

}
export const USER_TYPES = {
  FACULTY: 'faculty',
  STUDENT: 'student',
}