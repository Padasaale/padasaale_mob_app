import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { getDeviceWidth } from '../../../utils/CommonUtils'
import DTIcon from '../../../assets/icons/IconProvider';
import { SIZES, THEME, FONTS } from '../../../utils/ThemeProvider/ThemeProvider';

export const NoticeBoardContenet = (content) => {
  const { item, userType } = content;
  const { title, note, syllabus, course, classs, semister, userProfile } = item;

  const noticeBoardTitleView = (title) => {
    return (
      <View>
        <Text style={style.contentTitleText}>{title}</Text>
      </View>
    );
  }

  const noticeBoardNoteView = (note) => {
    return (
      <View>
        <Text numberOfLines={4} style={style.contentNoteText}>{note}</Text>
      </View>
    );
  }

  const classCourseView = (syllabus, course, classs, semister) => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: SIZES.margin3, marginBottom: SIZES.margin }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: THEME.colorPrimary2, marginHorizontal: SIZES.margin2, borderRadius: SIZES.borderRadius }}>
          <Text style={{ marginVertical: SIZES.margin, ...FONTS.h5, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{syllabus}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: THEME.colorPrimary2, marginHorizontal: SIZES.margin2, borderRadius: SIZES.borderRadius }}>
          <Text style={{ marginVertical: SIZES.margin, ...FONTS.h5, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{course}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: THEME.colorPrimary2, marginHorizontal: SIZES.margin2, borderRadius: SIZES.borderRadius }}>
          <Text style={{ marginVertical: SIZES.margin, ...FONTS.h5, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{classs}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: THEME.colorPrimary2, marginHorizontal: SIZES.margin2, borderRadius: SIZES.borderRadius }}>
          <Text style={{ marginVertical: SIZES.margin, ...FONTS.h5, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{semister}</Text>
        </View>
      </View>
    );
  }

  const profileView = (profileInfo) => {
    const { firstName, lastName, profilePicture, userType } = profileInfo;
    return (
      <View style={{ flexDirection: 'row' }}>
        <View>
          <Image source={{ uri: profilePicture }} style={{ height: 35, width: 35, borderWidth: SIZES.borderWidth, borderColor: THEME.colorPrimaryDark, borderRadius: SIZES.borderRadius }} />
        </View>
        <View style={{ justifyContent: 'center', marginHorizontal: SIZES.margin3 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ ...FONTS.h3_bold, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{firstName} </Text>
            <Text style={{ ...FONTS.h3_bold, textTransform: 'uppercase', color: THEME.colorAccent2 }}>{lastName}</Text>
          </View>
          <View>
            <Text style={{ ...FONTS.h6_bold, color: THEME.colorPrimaryDark, textTransform: 'capitalize' }}>{userType}</Text>
          </View>
        </View>
      </View>
    );
  }

  const userProfileAndAttachButtonView = () => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: SIZES.margin3, }}>
        <View>
          {profileView(userProfile)}
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TouchableOpacity>
            <View style={{ marginHorizontal: SIZES.margin3 }}>
              <DTIcon
                name='attach'
                size={20} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ marginHorizontal: SIZES.margin3 }}>
              <DTIcon
                name='activity'
                size={20} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  return (
    <View style={style.noticeBoardContentContainer}>
      {noticeBoardTitleView(title)}
      {noticeBoardNoteView(note)}
      {(userType === 'student') ? null : classCourseView(syllabus, course, classs, semister)}
      {userProfileAndAttachButtonView()}
    </View >
  );
}

const style = StyleSheet.create({
  noticeBoardContentContainer: {
    paddingHorizontal: SIZES.margin3,
    marginVertical: SIZES.margin2,
    borderBottomWidth: SIZES.borderWidth,
    borderColor: THEME.colorPrimaryDark,
    backgroundColor: THEME.colorPrimary,
    width: getDeviceWidth(),
  },

  contentTitleText: {
    ...FONTS.h3_bold,
    marginTop: SIZES.margin2,
    color: THEME.textPrimaryColor,
  },

  contentNoteText: {
    ...FONTS.h4,
    color: THEME.textPrimaryColor,
    marginVertical: SIZES.margin,
    lineHeight: 18,
  }
});