import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { NoticeBoardContenet } from './NoticeBoardContenet';
import lodash, { isEmpty } from 'lodash';
import { SIZES, THEME, FONTS } from '../../../utils/ThemeProvider/ThemeProvider';
import { USER_TYPES } from '../../common/Constants/Constants'
import { HOME_SCREEN_TABS_NAME } from '../../common/Constants/Constants';

export const NoticeBoard = (props) => {
  const { contents, userType, tabName } = props;

  const noticeBoardContents = ((() => {
    switch (tabName) {
      case HOME_SCREEN_TABS_NAME.ACTIVITY:
        return (contents) ? contents.map(content => ({
          title: content.title,
          note: content.activityNote,
          semister: (!isEmpty(content.semisters[0])) ? content.semisters[0].semister.semisterName : null,
          course: content.courseClasses[0].course.courseName,
          classs: content.courseClasses[0].academicClass.className,
          syllabus: content.syllabusCourses.syllabus.syllabusName,
          userProfile: {
            firstName: content.assignedBy.firstName,
            lastName: content.assignedBy.lastName,
            userType: content.assignedBy.userType,
            profilePicture: content.assignedBy.profilePicture,
          }
          // userProfile: ((() => {
          //   switch (userType) {
          //     case USER_TYPES.STUDENT:
          //       return {
          //         firstName: content.assignedBy.firstName,
          //         lastName: content.assignedBy.lastName,
          //         userType: content.assignedBy.userType,
          //         profilePicture: content.assignedBy.profilePicture,
          //       }
          //     case USER_TYPES.FACULTY:
          //       return {
          //         firstName: content.assignedTo[0].firstName,
          //         lastName: content.assignedTo[0].lastName,
          //         userType: content.assignedTo[0].userType,
          //         profilePicture: content.assignedTo[0].profilePicture,
          //       }
          //     default:
          //       break;
          //   }
          // })()),
        }
        )) : null;

      case HOME_SCREEN_TABS_NAME.CIRCULAR:
        return (contents) ? contents.map(content => {
          return {
            title: content.title,
            note: content.circularNote,
            semister: (!isEmpty(content.semisters[0])) ? content.semisters[0].semister.semisterName : null,
            course: content.courseClasses[0].course.courseName,
            classs: content.courseClasses[0].academicClass.className,
            syllabus: content.syllabusCourses ? content.syllabusCourses.syllabus.syllabusName : null,
            userProfile: {
              firstName: content.createdBy.firstName,
              lastName: content.createdBy.lastName,
              userType: content.createdBy.userType,
              profilePicture: content.createdBy.profilePicture,
            }
          }
        }) : null;

      case HOME_SCREEN_TABS_NAME.RESULT:
        return (contents) ? contents.map(content => {
          return {
            title: content.title,
            note: content.resultNote,
            semister: (content.semisters) ? (!isEmpty(content.semisters[0])) ? content.semisters[0].semister.semisterName : null : null,
            course: (content.courseClasses) ? content.courseClasses.course.courseName : null,
            classs: (content.courseClasses) ? content.courseClasses.academicClass.className : null,
            syllabus: content.syllabusCourses ? content.syllabusCourses.syllabus.syllabusName : null,
            userProfile: {
              firstName: content.assignedBy.firstName,
              lastName: content.assignedBy.lastName,
              userType: content.assignedBy.userType,
              profilePicture: content.assignedBy.profilePicture,
            }
          }
        }) : null;

      default:
        break;
    }
  })())

  return (
    <View>
      <FlatList
        data={noticeBoardContents}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => <NoticeBoardContenet item={item} index={index} userType={userType} />}
      />
    </View>
  );
}