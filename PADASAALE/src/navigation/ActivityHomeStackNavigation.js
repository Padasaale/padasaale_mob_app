import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ActivityHome from '../screens/home/activity/component/ActivityHomeComponent';

const ActivityHomeStack = createStackNavigator();

export default ActivityHomeStackNavigation = () => (
  <ActivityHomeStack.Navigator>
    <ActivityHomeStack.Screen name='ActivityHome' component={ActivityHome} options={{ title: false }} />
  </ActivityHomeStack.Navigator>
);