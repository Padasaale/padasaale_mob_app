import 'react-native-gesture-handler';
import React from 'react';
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import AuthStackNavigation from './AuthStackNavigation';
import HomeStackNavigation from '../navigation/HomeStackNavigation'

const NavigationStack = (props) => (
  <NavigationContainer>
    {props.isSignedIn ? (
      <HomeStackNavigation />
    ) : (
        <AuthStackNavigation />
      )}

  </NavigationContainer>
);

const mapStateToProps = ({ authData }) => ({
  isSignedIn: authData.isSignedIn,
});

export default connect(mapStateToProps)(NavigationStack);