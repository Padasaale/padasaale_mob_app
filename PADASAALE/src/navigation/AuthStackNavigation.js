import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignInComponent from '../screens/auth/component/SignInComponent'
import EmailRequestComponent from '../screens/auth/component/EmailRequestComponent'
import ResetPasswordComponent from '../screens/auth/component/ResetPasswordComponent';
import SignUpComponent from '../screens/auth/component/SignUpComponent';

const AuthStack = createStackNavigator();

export default AuthStackNavigation = () => (
  <AuthStack.Navigator initialRouteName='SignIn' headerMode='none'>
    <AuthStack.Screen name='SignIn' component={SignInComponent} />
    <AuthStack.Screen name='EmailRequest' component={EmailRequestComponent} />
    <AuthStack.Screen name='ResetPassword' component={ResetPasswordComponent} />
    <AuthStack.Screen name='SignUp' component={SignUpComponent} />
  </AuthStack.Navigator>
)
