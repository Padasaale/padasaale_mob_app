import React, { useState } from 'react'
import {
  View, TouchableOpacity, Dimensions, StyleSheet, Text, Animated
} from "react-native";
import DTIcon from '../../assets/icons/IconProvider';
import { SIZES, FONTS, THEME } from '../../utils/ThemeProvider/ThemeProvider'
import { HOME_SCREEN_TABS_NAME } from '../../screens/common/Constants/Constants';

export const TabBar = ({ state, descriptors, navigation, }) => {

  const [translateValue] = useState(new Animated.Value(0));

  const totalWidth = Dimensions.get('window').width;
  const tabWidth = totalWidth / state.routes.length;

  return (
    <View style={[style.tabContainer, { width: totalWidth - 16, height: 56 }]}>
      <View style={{ flexDirection: 'row' }}>
        <Animated.View style={[
          style.slider,
          {
            transform: [{ translateX: translateValue }],
            width: tabWidth - 32,
          },
        ]} />
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
                ? options.title
                : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
            Animated.spring(translateValue, {
              toValue: index * (tabWidth - 3),
              velocity: 2,
              useNativeDriver: true,
            }).start();
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityStates={isFocused ? ["selected"] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{ flex: 1 }}
              key={index}>
              <View style={{ height: "100%", justifyContent: "center", alignItems: 'center', marginTop: 0, marginBottom: SIZES.margin }}>
                <View style={{ backgroundColor: isFocused ? THEME.colorAccent : null, height: (35), width: (35), justifyContent: "center", alignItems: 'center', borderRadius: 35 / 2 }}>
                  <DTIcon
                    name={((() => {
                      switch (label) {
                        case HOME_SCREEN_TABS_NAME.ACTIVITY:
                          return 'activity'
                        case HOME_SCREEN_TABS_NAME.CIRCULAR:
                          return 'circular';
                        case HOME_SCREEN_TABS_NAME.RESULT:
                          return 'result';
                      }
                    }))()}
                    size={18}
                  />
                </View>
                <View>
                  <View>
                    <Text style={{ textTransform: 'uppercase', color: isFocused ? THEME.colorAccent2 : THEME.colorPrimaryDark, ...FONTS.h6 }}>
                      {label}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  tabContainer: {
    backgroundColor: THEME.colorPrimary,
    margin: SIZES.margin3,
    borderRadius: SIZES.borderRadius2,
    position: "absolute",
    bottom: 0,
    ...SIZES.card,
  },
  slider: {
    height: 2,
    position: "absolute",
    top: 0,
    left: 10,
    backgroundColor: "orange",
    borderRadius: 10,
    width: 50,
  },
});