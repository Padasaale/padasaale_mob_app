import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TabBar } from './TabBar';
import ActivityHomeStackNavigation from '../ActivityHomeStackNavigation';
import CircularHomeStackNavigation from '../CircularHomeStackNavigation';
import ResultHomeStackNavigation from '../ResultHomeStackNavigation';
import { HOME_SCREEN_TABS_NAME } from '../../screens/common/Constants/Constants';

const BottomTabBar = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      tabBar={props => <TabBar {...props} />}>
      <Tab.Screen name={HOME_SCREEN_TABS_NAME.ACTIVITY} component={ActivityHomeStackNavigation} />
      <Tab.Screen name={HOME_SCREEN_TABS_NAME.CIRCULAR} component={CircularHomeStackNavigation} />
      <Tab.Screen name={HOME_SCREEN_TABS_NAME.RESULT} component={ResultHomeStackNavigation} />
    </Tab.Navigator >
  );
}

export default BottomTabBar;