import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ResultHome from '../screens/home/result/component/ResultsHomeComponent';

const ResultHomeStack = createStackNavigator();

export default ResultHomeStackNavigation = () => (
  <ResultHomeStack.Navigator>
    <ResultHomeStack.Screen name='ResultHome' component={ResultHome} options={{ title: false }} />
  </ResultHomeStack.Navigator>
);