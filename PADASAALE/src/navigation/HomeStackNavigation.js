import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabBar from './BottomTabBar/BottomTabBar';

const HomeStack = createStackNavigator();

export default HomeStackNavigation = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name='BottomTabBar' component={BottomTabBar} options={{ headerShown: false }} />
  </HomeStack.Navigator>
);