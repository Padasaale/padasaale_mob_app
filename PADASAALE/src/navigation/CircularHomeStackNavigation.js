import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import CircularHome from '../screens/home/circular/component/CircularHomeComponent';

const CircularHomeStack = createStackNavigator();

export default CircularHomeStackNavigation = () => (
  <CircularHomeStack.Navigator>
    <CircularHomeStack.Screen name='CircularHome' component={CircularHome} options={{ title: false }} />
  </CircularHomeStack.Navigator>
);