import THEMES from './Themes.json';

export const themes = THEMES;
export const THEME = THEMES[1];

export const SIZES = {
  largeTitle: 22,
  largeTitle1: 28,
  largeTitle2: 34,

  h1: 18,
  h2: 16,
  h3: 14,
  h4: 12,
  h5: 10,
  h6: 8,

  padding: 2,
  padding2: 4,
  padding3: 8,

  margin: 2,
  margin2: 4,
  margin3: 8,

  borderRadius: 4,
  borderRadius2: 8,

  borderWidth: 0.5,
  borderWidth2: 1,

  opacity: 0.1,
  opacity2: 0.3,
  opacity3: 0.5,

  textOpacity: 0.7,

  card: {
    shadowColor: THEME.colorShadow,
    shadowOffset: {
      width: 2,
      height: 4,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  },
}

export const FONTS = {
  largeTitle: { fontFamily: "Inter-Regular", fontSize: SIZES.largeTitle },
  largeTitle1: { fontFamily: "Inter-Regular", fontSize: SIZES.largeTitle1 },
  largeTitle2: { fontFamily: "Inter-Regular", fontSize: SIZES.largeTitle2 },

  largeTitle_black: { fontFamily: "Inter-Black", fontSize: SIZES.largeTitle },
  largeTitle1_black: { fontFamily: "Inter-Black", fontSize: SIZES.largeTitle1 },
  largeTitle2_black: { fontFamily: "Inter-Black", fontSize: SIZES.largeTitle2 },

  largeTitle_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.largeTitle },
  largeTitle1_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.largeTitle1 },
  largeTitle2_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.largeTitle2 },

  h1: { fontFamily: "Inter-Regular", fontSize: SIZES.h1 },
  h2: { fontFamily: "Inter-Regular", fontSize: SIZES.h2 },
  h3: { fontFamily: "Inter-Regular", fontSize: SIZES.h3 },
  h4: { fontFamily: "Inter-Regular", fontSize: SIZES.h4 },
  h5: { fontFamily: "Inter-Regular", fontSize: SIZES.h5 },
  h6: { fontFamily: "Inter-Regular", fontSize: SIZES.h6 },

  h1_black: { fontFamily: "Inter-Black", fontSize: SIZES.h1 },
  h2_black: { fontFamily: "Inter-Black", fontSize: SIZES.h2 },
  h3_black: { fontFamily: "Inter-Black", fontSize: SIZES.h3 },
  h4_black: { fontFamily: "Inter-Black", fontSize: SIZES.h4 },
  h5_black: { fontFamily: "Inter-Black", fontSize: SIZES.h5 },
  h6_black: { fontFamily: "Inter-Black", fontSize: SIZES.h6 },

  h1_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h1 },
  h2_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h2 },
  h3_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h3 },
  h4_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h4 },
  h5_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h5 },
  h6_bold: { fontFamily: "Inter-Bold", fontSize: SIZES.h6 },
}

export const RANDOM_COLOR = () => {
  const colors = ['#4dd0e1', '#7986cb', '#ba68c8', '#e57373', '#4db6ac', '#aed581', '#9575cd', '#f06292', '#ff5252', '#ff7043'];
  return colors[Math.floor(Math.random() * colors.length)];
}


export const appTheme = { THEME, SIZES, FONTS, RANDOM_COLOR }