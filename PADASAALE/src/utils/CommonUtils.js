import React from 'react'
import { Dimensions, Linking, Platform } from 'react-native'

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get("window").height;

export const getDeviceWidth = () => (
    Math.floor(deviceWidth)
);

export const getDeviceHalfWidth = () => (
    Math.floor(deviceWidth / 2)
);

export const getDeviceHeight = () => (
    Math.floor(deviceHeight)
)

export const getFormatedDate = data => {
    const monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN",
        "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
    ];
    const date = new Date(data);
    return `${date.getDate()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`
}

export const getPresentDay = () => {
    const day = new Date().getDay();
    var weekday = new Array(7)
    weekday[0] = "sunday";
    weekday[1] = "monday";
    weekday[2] = "tuesday";
    weekday[3] = "wednesday";
    weekday[4] = "thursday";
    weekday[5] = "friday";
    weekday[6] = "saturday";
    return weekday[day];
}

export const getCurrentTime = () => {
    const time = new Date();
    return time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();
}

export const timeConvertion = (time) => {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
        time[0] = (time[0].toString().length === 1) ? '0' + time[0] : time[0];
        time[2] = (time[2].toString().length === 1) ? '0' + time[2] : time[2];
    }
    return time.join(''); // return adjusted time or original string
}

export const openMapUtility = ({ lat, lng }) => {
    const latlng = `${lat},${lng}`;
    const label = 'Custom Label';
    const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
    const url = Platform.select({
        ios: `${scheme}${label}@${latlng}`,
        android: `${scheme}${latlng}(${label})`,
    });
    Linking.openURL(url);
}