import AsyncStorage from '@react-native-async-storage/async-storage'
let instance = null;
class SharedPreferences {
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    async put(key, val) {
        try {
            return await AsyncStorage.setItem(key, val);
        } catch (error) {
            console.log('---> Unable to save item in preference', error);
            return;
        }
    }

    async remove(key) {
        try {
            return await AsyncStorage.removeItem(key);
        } catch (error) {
            console.log('---> Unable to remove item from preference', error);
            return;
        }
    }

    async removeAll() {
        try {
            const keys = await AsyncStorage.getAllKeys()
            return await AsyncStorage.multiRemove(keys);
        } catch (error) {
            console.log('Unable to remove from preferences: ', error);
        }
    }

    async get(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            console.log('--->Shared preference', value);
            return value;
        } catch (error) {
            console.log('---> Unable to get item from preference', error);
            return;
        }
    }

    getMultiple(keys, callback) {
        AsyncStorage.multiGet(keys, (err, stores) => {
            if (!err) {
                const keyVals = {};
                stores.map((result, i, store) => {
                    keyVals[`${result[0]}`] = result[1];
                });
                callback(keyVals);
            } else {
                callback([]);
            }
        });
    }
}

const sharedPreferences = new SharedPreferences();
export const SHARED_PREFERENCE = {
    accessor: sharedPreferences
};