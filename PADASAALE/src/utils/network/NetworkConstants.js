// export const APP_API_BASE_PATH = 'https://api.apttailor.com/api/';
export const APP_API_BASE_PATH = 'http://localhost:3001/api/';

export const NETWORK_TIME_OUT = 60000;
export const CACHE_MAX_AGE = 30;
export const STATUS = {
    SUCCESS: 'SUCCESS',
    FAIL: 'FAIL'
}
export const APP_ENVIRONMENT_TYPE = 'Development';